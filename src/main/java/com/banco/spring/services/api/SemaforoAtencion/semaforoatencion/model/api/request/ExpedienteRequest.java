package com.banco.spring.services.api.SemaforoAtencion.semaforoatencion.model.api.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ExpedienteRequest implements Serializable {

    private static final long serialVersionUID = 437078764518360770L;

    private String codeEjecutivo;
    private String codeExpediente;
}
