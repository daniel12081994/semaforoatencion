package com.banco.spring.services.api.SemaforoAtencion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

//@SpringBootApplication
@SpringBootApplication(exclude = {SecurityAutoConfiguration.class })
public class SemaforoAtencionApplication {

  public static void main(String[] args) {
    SpringApplication.run(SemaforoAtencionApplication.class, args);
  }

}
