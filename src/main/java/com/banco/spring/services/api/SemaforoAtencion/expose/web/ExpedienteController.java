package com.banco.spring.services.api.SemaforoAtencion.expose.web;

import com.banco.spring.services.api.SemaforoAtencion.semaforoatencion.business.ExpedienteService;
import com.banco.spring.services.api.SemaforoAtencion.semaforoatencion.model.api.request.ExpedienteRequest;
import com.banco.spring.services.api.SemaforoAtencion.semaforoatencion.model.api.response.ExpedienteResponse;
import io.reactivex.Single;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.DataBinder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/banco/service/api/expediente")
public class ExpedienteController {

    private final ExpedienteService expedienteService;

    public ExpedienteController(ExpedienteService expedienteService) {
        this.expedienteService = expedienteService;
    }

    @InitBinder
    public void initBinder(DataBinder binder) {
        binder.setDisallowedFields();
    }

    @ResponseStatus(HttpStatus.OK)
    @PostMapping(value = "/consulta", produces = {MediaType.APPLICATION_STREAM_JSON_VALUE,
            MediaType.APPLICATION_STREAM_JSON_VALUE})
    public Single<ExpedienteResponse> getConsulta(@RequestBody ExpedienteRequest expedienteRequest){
        return expedienteService.getConsulta(expedienteRequest);
    }


}
