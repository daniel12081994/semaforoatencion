package com.banco.spring.services.api.SemaforoAtencion.semaforoatencion.model.api.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.io.Serializable;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DatosEjecutivo implements Serializable {
    private static final long serialVersionUID = 437078764518360770L;

    private String codigo;
    private String nombres;
    private String apellidos;

}
