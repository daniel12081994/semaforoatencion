package com.banco.spring.services.api.SemaforoAtencion.semaforoatencion.model.api.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.math.BigDecimal;
import lombok.*;

import java.io.Serializable;
import java.sql.Timestamp;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Expedientes implements Serializable {

    private static final long serialVersionUID = 437078764518360770L;

    private String codigoExpediente;
    private String fechaAsignacion;
    private String fechaFinalizacion;
    private String expedienteEnAtencion;
    private Long horaDedicacion;
    private String semaforoAtencion;

}
