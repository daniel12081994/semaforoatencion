package com.banco.spring.services.api.SemaforoAtencion.repository;

import com.banco.spring.services.api.SemaforoAtencion.semaforoatencion.model.dbexpediente.Expediente;
import java.io.Serializable;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ExpedienteRepository extends MongoRepository<Expediente, Serializable> {


  @Query("{'codej': ?0}")
  List<Expediente> findByCodeej(String codeej);

  @Query("{'codej': ?0, 'code': ?1}")
  List<Expediente> findByCodeejAndCodeExp(String codeej, String codeExp);

}
