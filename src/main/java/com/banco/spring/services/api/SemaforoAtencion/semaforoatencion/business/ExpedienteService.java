package com.banco.spring.services.api.SemaforoAtencion.semaforoatencion.business;

import com.banco.spring.services.api.SemaforoAtencion.semaforoatencion.model.api.request.ExpedienteRequest;
import com.banco.spring.services.api.SemaforoAtencion.semaforoatencion.model.api.response.ExpedienteResponse;
import io.reactivex.Single;

public interface ExpedienteService {
    Single<ExpedienteResponse> getConsulta(ExpedienteRequest expedienteRequest);
}
