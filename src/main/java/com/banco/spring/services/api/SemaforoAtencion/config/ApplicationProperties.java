package com.banco.spring.services.api.SemaforoAtencion.config;

import java.util.Set;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
@Getter
@Setter
@ToString
public class ApplicationProperties {

  @Value("${application.config.ans.verde}")
  private Long ansVerde;

  @Value("${application.config.ans.amarillo}")
  private Long ansAmarillo;

  @Value("${application.config.horario.temprano_1_hh}")
  private Integer horaTempranoInicialHH;

  @Value("${application.config.horario.temprano_1_mm}")
  private Integer horaTempranoInicialMM;

  @Value("${application.config.horario.temprano_1_ss}")
  private Integer horaTempranoInicialSS;

  @Value("${application.config.horario.temprano_2_hh}")
  private Integer horaTempranoFinalHH;

  @Value("${application.config.horario.temprano_2_mm}")
  private Integer horaTempranoFinalMM;

  @Value("${application.config.horario.temprano_2_ss}")
  private Integer horaTempranoFinalSS;

  @Value("${application.config.horario.tarde_1_hh}")
  private Integer horaTardeInicialHH;

  @Value("${application.config.horario.tarde_1_mm}")
  private Integer horaTardeInicialMM;

  @Value("${application.config.horario.tarde_1_ss}")
  private Integer horaTardeInicialSS;

  @Value("${application.config.horario.tarde_2_hh}")
  private Integer horaTardeFinalHH;

  @Value("${application.config.horario.tarde_2_mm}")
  private Integer horaTardeFinalMM;

  @Value("${application.config.horario.tarde_2_ss}")
  private Integer horaTardeFinalSS;

  @Value("${application.config.atendido.proceso}")
  private String atencionProceso;

  @Value("${application.config.atendido.fin}")
  private String atencionFinalizado;

  @Value("${application.config.ans.msm_verde}")
  private String msmAnsVerde;

  @Value("${application.config.ans.msm_amarillo}")
  private String msmAnsAmarillo;

  @Value("${application.config.ans.msm_rojo}")
  private String msmAnsRojo;

  @Value("${application.config.horario.feriados}")
  private Set<String> feriados;

  @Value("${application.config.horario.horas_laborales}")
  private Long horasLaborales;

  @Value("${application.config.horario.dia_domingo}")
  private Integer diaDomingo;

  @Value("${application.config.atendido.no_ejecutivo}")
  private String messageEjecutivo;
}
