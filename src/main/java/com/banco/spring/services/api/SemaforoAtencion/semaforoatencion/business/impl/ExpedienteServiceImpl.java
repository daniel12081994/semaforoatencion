package com.banco.spring.services.api.SemaforoAtencion.semaforoatencion.business.impl;

import com.banco.spring.services.api.SemaforoAtencion.repository.EjecutivoRepository;
import com.banco.spring.services.api.SemaforoAtencion.repository.ExpedienteRepository;
import com.banco.spring.services.api.SemaforoAtencion.semaforoatencion.business.ExpedienteService;
import com.banco.spring.services.api.SemaforoAtencion.semaforoatencion.business.processor.ExpedienteResponseProcessor;
import com.banco.spring.services.api.SemaforoAtencion.semaforoatencion.model.api.request.ExpedienteRequest;
import com.banco.spring.services.api.SemaforoAtencion.semaforoatencion.model.api.response.ExpedienteResponse;
import com.banco.spring.services.api.SemaforoAtencion.semaforoatencion.model.dbexpediente.Ejecutivo;
import com.banco.spring.services.api.SemaforoAtencion.semaforoatencion.model.dbexpediente.Expediente;
import io.reactivex.Single;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class ExpedienteServiceImpl implements ExpedienteService {

  private final EjecutivoRepository ejecutivoRepository;
  private final ExpedienteRepository expedienteRepository;
  private final ExpedienteResponseProcessor expedienteResponseProcessor;

  public ExpedienteServiceImpl(EjecutivoRepository ejecutivoRepository,
      ExpedienteRepository expedienteRepository,
      ExpedienteResponseProcessor expedienteResponseProcessor) {
    this.ejecutivoRepository = ejecutivoRepository;
    this.expedienteRepository = expedienteRepository;
    this.expedienteResponseProcessor = expedienteResponseProcessor;
  }

  @Override
  public Single<ExpedienteResponse> getConsulta(ExpedienteRequest expedienteRequest) {
    log.info("===Request: " + expedienteRequest);
    log.info("===Search in DB===");
    return Single.zip(Single.just(getEjecutivo(expedienteRequest.getCodeEjecutivo())),
        Single.just(getExpedientes(expedienteRequest)),
        expedienteResponseProcessor::process);
  }

  private Ejecutivo getEjecutivo(String code) {
    Ejecutivo ejecutivo = ejecutivoRepository.findByCode(code);
    return ejecutivo == null ? new Ejecutivo() : ejecutivo;
  }

  private List<Expediente> getExpedientes(ExpedienteRequest expedienteRequest) {
    return expedienteRequest.getCodeExpediente() == null ||
        expedienteRequest.getCodeExpediente().isEmpty() ?
        expedienteRepository.findByCodeej(expedienteRequest.getCodeEjecutivo()) :
        expedienteRepository.findByCodeejAndCodeExp(expedienteRequest.getCodeEjecutivo(),
            expedienteRequest.getCodeExpediente());
  }
}
