package com.banco.spring.services.api.SemaforoAtencion.repository;

import com.banco.spring.services.api.SemaforoAtencion.semaforoatencion.model.dbexpediente.Ejecutivo;
import java.io.Serializable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EjecutivoRepository extends MongoRepository<Ejecutivo, Serializable> {

  Ejecutivo findByCode(String code);

}
