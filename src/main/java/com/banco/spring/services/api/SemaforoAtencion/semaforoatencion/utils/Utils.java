package com.banco.spring.services.api.SemaforoAtencion.semaforoatencion.utils;

import static java.time.format.DateTimeFormatter.ISO_DATE_TIME;

import com.banco.spring.services.api.SemaforoAtencion.config.ApplicationProperties;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Utils {

  private Utils() {
    throw new IllegalStateException("Utility class");
  }

  public static String buildSemaforo(Long hora, ApplicationProperties applicationProperties) {
    if (hora.doubleValue() <= applicationProperties.getAnsVerde()) {
      return applicationProperties.getMsmAnsVerde();
    } else if (hora.doubleValue() <= applicationProperties.getAnsAmarillo()) {
      return applicationProperties.getMsmAnsAmarillo();
    } else {
      return applicationProperties.getMsmAnsRojo();
    }
  }

  public static Long getHora(String fechaasig, String fechafin,
      ApplicationProperties applicationProperties) {
    LocalDateTime from = LocalDateTime.parse(fechaasig, ISO_DATE_TIME);
    LocalDateTime to = fechafin.isEmpty() ? LocalDateTime.now() : LocalDateTime.parse(fechafin,
        ISO_DATE_TIME);

    Date dateFrom = Date.from(from.atZone(ZoneId.systemDefault()).toInstant());
    Date dateTo = Date.from(to.atZone(ZoneId.systemDefault()).toInstant());
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    if (sdf.format(dateFrom).equalsIgnoreCase(sdf.format(dateTo))) {
      sdf = new SimpleDateFormat("HH:mm:ss");
      LocalTime end = LocalTime.parse(sdf.format(dateTo));
      LocalTime horaTempranoFinal = LocalTime.of(applicationProperties.getHoraTempranoFinalHH(),
          applicationProperties.getHoraTempranoFinalMM(),
          applicationProperties.getHoraTempranoFinalSS());
      if (end.isBefore(horaTempranoFinal)) {
        return ChronoUnit.HOURS.between(from, to);
      } else {
        return ChronoUnit.HOURS.between(from, to) - 1;
      }
    } else {
      sdf = new SimpleDateFormat("HH:mm:ss");
      SimpleDateFormat day = new SimpleDateFormat("yyyy-MM-dd");
      Long days = ChronoUnit.DAYS.between(from, to) + 1;
      Long count = 0L;
      for (int i = 1; i <= days; i++) {
        if (i == 1) {
          LocalTime begin = LocalTime.parse(sdf.format(dateFrom));
          LocalTime horaTempranoFinal = LocalTime.of(applicationProperties.getHoraTempranoFinalHH(),
              applicationProperties.getHoraTempranoFinalMM(),
              applicationProperties.getHoraTempranoFinalSS());
          if (begin.isBefore(horaTempranoFinal)) {
            count = count + (ChronoUnit.HOURS.between(LocalTime.parse(sdf.format(dateFrom)),
                LocalTime.of(applicationProperties.getHoraTardeFinalHH(),
                    applicationProperties.getHoraTardeFinalMM(),
                    applicationProperties.getHoraTardeFinalSS())) - 1);
          } else {
            count = count + ChronoUnit.HOURS.between(LocalTime.parse(sdf.format(dateFrom)),
                LocalTime.of(applicationProperties.getHoraTardeFinalHH(),
                    applicationProperties.getHoraTardeFinalMM(),
                    applicationProperties.getHoraTardeFinalSS()));
          }
        } else if (i == days) {
          LocalTime end = LocalTime.parse(sdf.format(dateTo));
          LocalTime horaTempranoFinal = LocalTime.of(applicationProperties.getHoraTempranoFinalHH(),
              applicationProperties.getHoraTempranoFinalMM(),
              applicationProperties.getHoraTempranoFinalSS());
          LocalTime horaTardeFinal = LocalTime.of(applicationProperties.getHoraTardeFinalHH(),
              applicationProperties.getHoraTardeFinalMM(),
              applicationProperties.getHoraTardeFinalSS());
          if (end.isBefore(horaTempranoFinal)) {
            count = count + ChronoUnit.HOURS.between(LocalTime.of(applicationProperties.getHoraTempranoInicialHH(),
                applicationProperties.getHoraTempranoInicialMM(),
                applicationProperties.getHoraTempranoInicialSS()), end);
          } else if (end.isAfter(horaTardeFinal)) {
            count = count + applicationProperties.getHorasLaborales();
          } else {
            count = count + (ChronoUnit.HOURS.between(LocalTime.of(applicationProperties.getHoraTempranoInicialHH(),
                applicationProperties.getHoraTempranoInicialMM(),
                applicationProperties.getHoraTempranoInicialSS()), end) - 1);
          }
        } else {
          dateFrom = Date.from(from.plusDays(i - 1).atZone(ZoneId.systemDefault()).toInstant());
          if (applicationProperties.getFeriados().contains(day.format(dateFrom)) ||
              from.plusDays(i - 1).getDayOfWeek().getValue() == applicationProperties.getDiaDomingo()) {
            log.info("===Domingo o feriado===");
            continue;
          }
          count = count + applicationProperties.getHorasLaborales();
        }
      }
      return count;
    }
  }

  public static String formatDate(String fecha) {
    LocalDateTime from = LocalDateTime.parse(fecha, ISO_DATE_TIME);
    Date date = Date.from(from.atZone(ZoneId.systemDefault()).toInstant());
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    return sdf.format(date);
  }

}
