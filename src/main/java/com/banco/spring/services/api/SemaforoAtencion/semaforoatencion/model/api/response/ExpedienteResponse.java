package com.banco.spring.services.api.SemaforoAtencion.semaforoatencion.model.api.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ExpedienteResponse implements Serializable {

    private static final long serialVersionUID = 437078764518360770L;

    private DatosEjecutivo ejecutivo;
    private List<Expedientes> expedientes;
    private String mensaje;

}
