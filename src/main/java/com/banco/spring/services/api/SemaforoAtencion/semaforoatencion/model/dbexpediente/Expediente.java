package com.banco.spring.services.api.SemaforoAtencion.semaforoatencion.model.dbexpediente;

import java.io.Serializable;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "coll_expediente")
public class Expediente implements Serializable {

  @Id
  @NotNull
  private String id;
  private String code;
  private String codej;
  private String fechaasig;
  private String fechafin;

}
