package com.banco.spring.services.api.SemaforoAtencion.semaforoatencion.business.processor;

import com.banco.spring.services.api.SemaforoAtencion.config.ApplicationProperties;
import com.banco.spring.services.api.SemaforoAtencion.semaforoatencion.model.api.response.DatosEjecutivo;
import com.banco.spring.services.api.SemaforoAtencion.semaforoatencion.model.api.response.ExpedienteResponse;
import com.banco.spring.services.api.SemaforoAtencion.semaforoatencion.model.api.response.Expedientes;
import com.banco.spring.services.api.SemaforoAtencion.semaforoatencion.model.dbexpediente.Ejecutivo;
import com.banco.spring.services.api.SemaforoAtencion.semaforoatencion.model.dbexpediente.Expediente;
import com.banco.spring.services.api.SemaforoAtencion.semaforoatencion.utils.Utils;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ExpedienteResponseProcessor {

  private final ApplicationProperties applicationProperties;

  public ExpedienteResponseProcessor(ApplicationProperties applicationProperties) {
    this.applicationProperties = applicationProperties;
  }

  public ExpedienteResponse process(Ejecutivo ejecutivo, List<Expediente> expedientes) {
    if (ejecutivo.getId() == null) {
      return ExpedienteResponse.builder().mensaje(applicationProperties.getMessageEjecutivo()).build();
    } else {
      return ExpedienteResponse
          .builder()
          .ejecutivo(buildEjecutivo(ejecutivo))
          .expedientes(buildExpedientes(expedientes))
          .build();
    }
  }

  private List<Expedientes> buildExpedientes(List<Expediente> expedientes) {
    return expedientes.stream()
        .map(ex -> {
          Long hora = Utils.getHora(ex.getFechaasig(), ex.getFechafin(), applicationProperties);
          return Expedientes
              .builder()
              .codigoExpediente(ex.getCode())
              .fechaAsignacion(Utils.formatDate(ex.getFechaasig()))
              .fechaFinalizacion(ex.getFechafin().isEmpty() ? null : Utils.formatDate(ex.getFechafin()))
              .expedienteEnAtencion(ex.getFechafin().isEmpty() ? applicationProperties.getAtencionProceso() :
                  applicationProperties.getAtencionFinalizado())
              .horaDedicacion(hora)
              .semaforoAtencion(Utils.buildSemaforo(hora, applicationProperties))
              .build();
        })
        .collect(Collectors.toList());
  }

  private DatosEjecutivo buildEjecutivo(Ejecutivo ejecutivo) {
    return DatosEjecutivo
        .builder()
        .codigo(ejecutivo.getCode())
        .nombres(ejecutivo.getName())
        .apellidos(ejecutivo.getLastname())
        .build();
  }

}
